<?php

return [

	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session',

	/**
	 * Consumers
	 */
	'consumers' => [

		'Facebook' => [
			'client_id'     => '919336581455896',
			'client_secret' => '0ef6d07b33d68fa4ef96efc2d40aac58',
			'scope'         => ['email','user_about_me'],
		],

	]

];