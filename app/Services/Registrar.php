<?php namespace App\Services;

use App\Profile;
use App\User;
use Illuminate\Support\Str;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'first_name' => 'required|max:255',
			'last_name' => 'required|max:255',
			'gender' => 'required',
			'mobile' => 'required|numeric',
			'address' => 'required',
			'email' => 'required|email|max:255|unique:users,email',
			'password' => 'required|min:6',
            'confirm_password' => 'required|same:password'
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
        //Insert user
		$user = User::create([
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
            'status' => 'active'
		]);

        //Insert profile
        $profile = Profile::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'gender' => $data['gender'],
            'mobile' => $data['mobile'],
            'address' => $data['address']
        ]);

        //Update slug
        $user->slug = Str::slug($user->id.' '.$profile->first_name.' '.$profile->last_name);

        $profile->user()->associate($user);
        $profile->save();
        $user->save();

        return $user;
	}

}
