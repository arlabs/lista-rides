<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');
Route::get('loginWithFacebook', 'FacebookController@loginWithFacebook');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

//region Types
Route::get('types', 'TypesController@index');
Route::get('types/{slug}', 'TypesController@show');
//endregion

Route::group(['middleware' => 'auth'], function(){
    Route::get('home', 'HomeController@index');

    //region Profile
    Route::get('profile/{slug}', 'ProfilesController@show');
    Route::post('profile/{slug}', 'ProfilesController@update');
    //endregion

    //region Vehicles
    Route::get('vehicles', 'VehiclesController@index');
    Route::get('vehicles/create', 'VehiclesController@create');
    Route::post('vehicles/store', 'VehiclesController@store');
    Route::get('vehicles/{slug}', 'VehiclesController@show');
    Route::get('vehicles/{slug}/edit', 'VehiclesController@edit');
    Route::post('vehicles/{slug}', 'VehiclesController@update');
    Route::get('vehicles/{slug}/verify', 'VehiclesController@verify');
    Route::post('vehicles/{slug}/verify', 'VehiclesController@verify');
    //endregion

    //region Inquiries
    Route::get('inquiries', 'InquiriesController@index');
    Route::post('inquiries/store', 'InquiriesController@store');
    Route::get('inquiries/{id}/action', 'InquiriesController@action');
    //endregion

    //region Feedback
    Route::get('feedback/create', 'FeedbacksController@create');
    Route::post('feedback/store', 'FeedbacksController@store');
    //endregion

    //region Uploads
    Route::get('uploads/loader', 'UploadsController@loader');
    Route::post('uploads/upload', 'UploadsController@upload');
    //endregion
});