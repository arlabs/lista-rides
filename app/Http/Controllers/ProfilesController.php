<?php namespace App\Http\Controllers;

use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfilesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function show($slug){
        try{
            $user = User::with(['profile'])->where('slug',$slug)->firstOrFail();
        }catch (ModelNotFoundException $e){
            return redirect('home');
        }

        return view('profile.index', compact('user'));
    }

	public function update(Request $request, $slug)
	{
        if($request->ajax()){
            $data = $request->get('data');
            try {
                $user = User::with(['profile'])->where('slug',$slug)->firstOrFail();

                $validator = Validator::make($data, [
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'gender' => 'required',
                    'mobile' => 'required|numeric',
                    'address' => 'required'
                ]);

                if($validator->fails()){
                    $return = [
                        'success' => false,
                        'errors' => $validator->errors(),
                        'data' => null,
                        'type' => 'error',
                        'message' => 'An error occurred when submitting the form.'
                    ];
                } else {

                    if ($data['first_name'] != "") $user->profile->first_name = $data['first_name'];
                    if ($data['last_name'] != "") $user->profile->last_name = $data['last_name'];
                    if ($data['gender'] != "") $user->profile->gender = $data['gender'];
                    if ($data['mobile'] != "") $user->profile->mobile = $data['mobile'];
                    if ($data['address'] != "") $user->profile->address = $data['address'];
                    $user->profile->save();

                    $return = [
                        'success' => true,
                        'errors' => null,
                        'data' => $user,
                        'type' => 'success',
                        'message' => 'You\'ve successfully edited your info.'
                    ];
                }

            } catch (ModelNotFoundException $e) {
                $return = [
                    'success' => false,
                    'errors' => null,
                    'data' => null,
                    'type' => '',//Warning (default of PNotify)
                    'message' => 'User not found.'
                ];
            }

            return $return;
        }

        return null;
	}

}
