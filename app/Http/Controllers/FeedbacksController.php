<?php namespace App\Http\Controllers;

use App\Feedback;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Inquiry;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class FeedbacksController extends Controller {

	public function create(Request $request){

        $view = View::make('feedbacks._create', [
            'id' => $request->get('id'),
            'status' => $request->get('status')
        ]);
        $content = $view->render();

        return $content;
    }

    public function store(Request $request){
        $data = $request->get('data');

        $validator = Validator::make($request->all(), [
            'rating' => 'required|numeric',
            'message' => 'required',
            'id' => 'required',
            'status' => 'required'
        ]);

        if($validator->fails()){
            $return = ['success' => false, 'data' => null, 'errors' => $validator->errors(), 'message' => null];
        } else {
            try {
                $inquiry = Inquiry::where('id',$request->get('id'))->firstOrFail();
                $status = $request->get('status');
            } catch (ModelNotFoundException $e) {
                return ['success' => false, 'data' => null, 'errors' => null, 'message' => 'Inquiry not found.'];
            }

            if(in_array($status,['partially_returned','returned'])) {
                $feedback = new Feedback();
                if ($status == 'partially_returned') {
                    //lister
                    $feedback->user_id = $inquiry->vehicle->user_id;
                    $feedback->account_type = 'lister';
                } elseif ($status == 'returned') {
                    //renter
                    $feedback->user_id = $inquiry->renter_id;
                    $feedback->account_type = 'renter';
                }

                //Inquiry
                Inquiry::takeAction($inquiry->id,$request);

                $feedback->rating = $request->get('rating');
                $feedback->message = $request->get('message');
                $feedback->save();
            } else {
                return ['success' => false, 'data' => null, 'errors' => null, 'message' => 'Status not found.'];
            }

            $return = ['success' => true, 'data' => null, 'errors' => null, 'message' => 'Feedback successfully sent. Thank you for giving your time to send your feedback.'];
        }

        return $return;
    }

}
