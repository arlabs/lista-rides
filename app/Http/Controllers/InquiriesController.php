<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Inquiry;
use App\Vehicle;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class InquiriesController extends Controller {

	public function index(){
        $inquiries = Inquiry::with(['vehicle','renter'])
            ->whereHas('vehicle', function($query){
                $query->where('vehicles.user_id',Auth::id())
                    ->orWhere('renter_id',Auth::id());
            })
            ->paginate(10);

        return view('inquiries.index', compact('inquiries'));
    }

    public function store(Request $request){
        if($request->ajax()){
            $validator = Validator::make($request->all(), [
                'start' => 'required|date',
                'end' => 'required|date|after:start',
                'price_type' => 'required'
            ]);

            if($validator->fails()){
                $return = ['success' => false, 'data' => null, 'errors' => $validator->errors(), 'message' => null];
            } else {
                try {
                    $vehicle = Vehicle::with(['type','inquiry'])->where('id',$request->get('vehicle_id'))->firstOrFail();
                } catch (ModelNotFoundException $e) {
                    return ['success' => false, 'data' => null, 'errors' => null, 'type' => 'error', 'message' => 'Vehicle not found.'];
                }

                $start = date('Y-m-d H:i:s', strtotime($request->get('start')));
                $end = date('Y-m-d H:i:s', strtotime($request->get('end')));
                $borrowed = Inquiry::checkIfBorrowed($vehicle->id,$start,$end);

                if($borrowed){
                    return ['success' => false, 'data' => null, 'errors' => null, 'type' => '', 'message' => 'Someone has already borrowed the vehicle between these schedules. Try another one.'];
                }

                $inquiry = new Inquiry();
                $inquiry->vehicle_id = $vehicle->id;
                $inquiry->renter_id = Auth::id();
                $inquiry->start = $start;
                $inquiry->end = $end;
                $inquiry->price_type = $request->get('price_type');
                $inquiry->save();

                $return = ['success' => true, 'data' => $inquiry, 'errors' => null, 'message' => 'Your inquiry was successfully sent to owner. You may check your inquiry to Inquiries section.'];
            }

            return $return;
        }

        return null;
    }

    public function action($id, Request $request){
        Inquiry::takeAction($id,$request);

        return redirect('inquiries');
    }

}
