<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Vehicle;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;

class UploadsController extends Controller {

	public function loader(Request $request){
        $view = View::make('uploads._load', ['slug' => $request->get('slug'), 'purpose' => $request->get('purpose')]);
        $content = $view->render();

        return $content;
    }

    public function upload(Request $request){
        $valid_exts = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
        $max_size = 2000 * 1024; // max file size (200kb)
        $path = public_path() . '/img/'; // upload directory
        $fileName = NULL;

        if ( $_SERVER['REQUEST_METHOD'] === 'POST' )
        {
            //Get the file input
            $file = Input::file('image');

            //Check if file is not empty
            if($file){
                //If file doesn't exist, create it
                if (!file_exists($path)) {
                    mkdir($path, 0777);
                }

                //Get extension
                $ext = $file->getClientOriginalExtension();

                //Get file size
                $size = $file->getSize();

                //Create a unique name
                $name = time() . '_' . (rand(1, 999)) . '.' . $ext;

                if (in_array($ext, $valid_exts) AND $size < $max_size) {
                    if ($request->has('slug') AND $request->has('purpose')) {
                        // move uploaded file from temp to uploads directory
                        if ($file->move($path, $name)) {
                            $slug = $request->get('slug');
                            //Image::make(sprintf($path.'/%s', $name))->resize(245, 245)->save();
                            $fileName = $name;
                            $px = 0;
                            if ($request->get('purpose') == 'vehicle') {
                                $px = 470;
                                try {
                                    $vehicle = Vehicle::where('slug', $slug)->firstOrFail();
                                    $vehicle->image = $fileName;
                                    $vehicle->save();
                                } catch (ModelNotFoundException $e) {
                                    return ['success' => false, 'message' => 'Vehicle not found.'];
                                }
                            }
                            elseif ($request->get('purpose') == 'profile') {
                                $px = 254;
                                try {
                                    $user = User::where('slug', $slug)->firstOrFail();
                                    $user->profile->image = $fileName;
                                    $user->profile->save();
                                } catch (ModelNotFoundException $e) {
                                    return ['success' => false, 'message' => 'Vehicle not found.'];
                                }
                            }
                            else {
                                $return = ['success' => false, 'message' => 'Upload Fail: No purpose.'];
                            }

                            $return = ['success' => true, 'message' => 'Image successfully uploaded.'];
                            Image::make(sprintf($path . '/%s', $name))->fit($px)->save();
                        } else {
                            $return = ['success' => false, 'message' => 'Upload Fail: File not uploaded.'];
                        }
                    } else {
                        $return = ['success' => false, 'message' => 'Upload Fail: Unknown vehicle or no purpose.'];
                    }
                } else {
                    if ($size < $max_size) {
                        $return = ['success' => false, 'message' => 'Upload Fail: Maximum file size is 200kb.'];
                    } else {
                        $return = ['success' => false, 'message' => 'Upload Fail: Unsupported file format.'];
                    }
                }
            } else {
                $return = ['success' => false, 'message' => 'No file selected.'];
            }
        }
        else { $return = ['success' => false, 'message' => 'Bad request.']; }

        return $return;
    }

}