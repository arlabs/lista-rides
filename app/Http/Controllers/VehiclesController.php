<?php namespace App\Http\Controllers;

use App\Feature;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Type;
use App\Vehicle;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class VehiclesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        if(Auth::user()->account_type == 'admin'){
            $Vehicles = Vehicle::get_latest()->paginate(8);
        } else {
            $Vehicles = Vehicle::where('user_id',Auth::id())->paginate(8);
        }

        return view('vehicles.index', compact('Vehicles'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $types = Type::lists('name','id');
        $url = 'vehicles/store';
        $features = Feature::all();
        $vehicleFeature = [];

        $view = View::make('vehicles._create-edit', ['vehicle' => null, 'types' => $types, 'vehicleFeature' => $vehicleFeature, 'features' => $features, 'url' => $url]);
        $content = $view->render();

		return $content;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        if($request->ajax()) {
            $return = Vehicle::list_vehicle($request);

            return $return;
        }

        return null;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		try {
            $vehicle = Vehicle::with(['type'])->where('slug',$slug)->firstOrFail();
            if((Auth::user()->account_type == 'user' and Auth::id() == $vehicle->user_id) or (Auth::user()->account_type == 'admin')){
                $isOwnerOrAdmin = true;
            } else {
                $isOwnerOrAdmin = false;
            }
        } catch (ModelNotFoundException $e) {
            return redirect('vehicles');
        }

        return view('vehicles.show', compact('vehicle','isOwnerOrAdmin'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($slug)
	{
        try {
            $vehicle = Vehicle::where('slug',$slug)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            $vehicle = null;
        }
        $types = Type::lists('name','id');
        $url = 'vehicles/'.$vehicle->slug;
        $features = Feature::all();

        //Vehicle features
        $vehicleFeature = [];
        foreach ($vehicle->feature as $feature) {
            $vehicleFeature[] = $feature->name;
        }

        $view = View::make('vehicles._create-edit', ['vehicle' => $vehicle, 'types' => $types, 'vehicleFeature' => $vehicleFeature, 'features' => $features, 'url' => $url]);
        $content = $view->render();

        return $content;
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $slug)
	{
        if($request->ajax()) {
            $return = Vehicle::list_vehicle($request,$slug,'edit');

            return $return;
        }

        return null;
	}

    public function verify($slug, Request $request){
        try {
            $vehicle = Vehicle::where('slug',$slug)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return ['success' => false, 'message' => 'Vehicle not found.'];
        }

        if ($request->isMethod('post')) {
            if($request->get('action') == 'verify') {
                $vehicle->verified = 1;
            } else {
                $vehicle->verified = 0;
            }
            $vehicle->save();

            return ['success' => true, 'message' => 'Vehicle successfully '.str_replace('y','ied',$request->get('action')).'.'];
        } else {
            $view = View::make('vehicles._verify', ['slug' => $vehicle->slug, 'request' => $request]);
            $content = $view->render();

            return $content;
        }
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
