<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Type;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TypesController extends Controller {

	public function index(){
        $Types = Type::paginate(8);

        return view('types.index', compact('Types'));
    }

    public function show($slug){
        try {
            $Type = Type::with(['vehicle'])->where('slug',$slug)->firstOrFail();

            if (Auth::check()) {
                $Vehicles = $Type->vehicle()->where('verified','=',1)->where('user_id','!=',Auth::id())->paginate(8);
            } else {
                $Vehicles = $Type->vehicle()->where('verified','=',1)->paginate(8);
            }
        } catch (ModelNotFoundException $e) {
            return redirect('types');
        }

        return view('types.show', compact('Type','Vehicles'));
    }

}
