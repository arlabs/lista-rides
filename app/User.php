<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['email','password','slug','status','account_type'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    public function profile(){
        return $this->hasOne('App\Profile');
    }

    public function inquiry(){
        return $this->hasMany('App\Inquiry','renter_id','id');
    }

    public function vehicle(){
        return $this->hasMany('App\Vehicle','user_id','id');
    }

    public function feedback(){
        return $this->hasMany('App\Feedback','user_id','id');
    }

    public function getFeedbackAverage(){
        $listerFeedback = $this->feedback()->where('account_type','like','%lister%');
        $total = $listerFeedback->count();
        $num = 0;
        if($total > 0){
            foreach ($this->feedback as $feedback) {
                $num += $feedback->rating;
            }
            $a = $num / $total;

            return $a;
        } else {
            return 0;
        }
    }

    public function getFeedbackAverageWithStar(){
        $ave = $this->getFeedbackAverage();
        $star = '';

        for ($i=1; $i<=5; $i++){
            if ($i <= $ave) {
                $star .= '<i class="fa fa-star"></i>';
            } else {
                $star .= '<i class="fa fa-star-o"></i>';
            }
        }

        return $star;
    }

}
