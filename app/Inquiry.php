<?php
namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Inquiry extends Model{

    protected $fillable = ['vehicle_id','renter_id','start','end','price_type','status'];

    public function vehicle(){
        return $this->belongsTo('App\Vehicle','vehicle_id');
    }

    public function renter(){
        return $this->belongsTo('App\User','renter_id','id');
    }

    public function getPrice(){
        if(empty($this->price_type)) {
            $value = null;
        } else {
            if ($this->price_type == 'per_hour') {
                $value = number_format($this->vehicle->per_hour,2);
            } elseif ($this->price_type == 'per_day') {
                $value = number_format($this->vehicle->per_day,2);
            } else {
                $value = number_format($this->vehicle->per_week,2);
            }
        }

        return $value;
    }

    public static function checkIfBorrowed($vehicleID, $start, $end){
        $borrowed = Inquiry::with(['vehicle','renter'])
            ->where('vehicle_id','=',$vehicleID)
            ->where('status','=','accepted')
            ->where(function($query) use($start,$end) {
                $query->where(function($q) use($start,$end){
                    $q->where('start','<=',$start) //Inputted dates are between DB dates
                    ->where('end','>=',$end);
                })
                    ->orWhere(function($q) use($start,$end){
                        $q->where('start','<=',$start) //Inputted start is between DB dates
                        ->where('end','>=',$start);
                    })
                    ->orWhere(function($q) use($start,$end){
                        $q->where('start','<=',$end) //Inputted end is between DB dates
                        ->where('end','>=',$end);
                    })
                    ->orWhere(function($q) use($start,$end){
                        $q->where('start','>=',$start) //DB dates are between inputted dates
                        ->where('end','<=',$end);
                    });
            })
            ->first();

        return $borrowed;
    }

    public static function takeAction($inquiry_id, $request){
        $status = $request->get('status');

        try {
            $inquiry = Inquiry::with(['vehicle','renter'])->where('id',$inquiry_id)->firstOrFail();
            $inquiry->status = $status;
            $inquiry->save();
        } catch (ModelNotFoundException $e) {
            if($request->ajax())
                return $e;
            else
                return redirect('inquiries');
        }
    }

} 