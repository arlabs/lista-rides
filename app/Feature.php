<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model{

    protected $fillable = ['name'];

    public function vehicle(){
        return $this->belongsToMany('App\Vehicle','vehicle_features');
    }

} 