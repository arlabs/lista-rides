<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class Vehicle extends Model{

    protected $fillable = ['type_id','user_id','name','description','per_hour','per_day','per_week','image','slug'];

    public function type(){
        return $this->belongsTo('App\Type','type_id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function feature(){
        return $this->belongsToMany('App\Feature','vehicle_features');
    }

    public function inquiry(){
        return $this->hasMany('App\Inquiry','vehicle_id','id');
    }

    public static function get_latest(){
        return self::latest('created_at');
    }

    public function verification_status(){
        if($this->verified){
            $html = '<span class="text-success" data-toggle="tooltip" title="Verified"><i class="fa fa-check-circle"></i></span>';
        } else {
            $html = '<span class="text-danger" data-toggle="tooltip" title="Unverified"><i class="fa fa-exclamation-circle"></i></span>';
        }

        return $html;
    }

    public static function list_vehicle($request, $slug = '', $type = 'create'){
        $data = $request->get('data');

        $validator = Validator::make($data, [
            'name' => 'required',
            'type' => 'required',
            'chassis_number' => 'required',
            'plate_number' => 'required',
            'description' => 'required',
            'per_hour' => 'required|numeric',
            'per_day' => 'required|numeric',
            'per_week' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            $return = ['success' => false, 'data' => null, 'errors' => $validator->errors()];
        } else {
            if($type == 'create') {
                $vehicle = new Vehicle();
            } else {
                try {
                    $vehicle = Vehicle::with(['type'])->where('slug',$slug)->firstOrFail();
                } catch (ModelNotFoundException $e) {
                    return ['success' => false, 'data' => null, 'errors' => null, 'message' => 'Vehicle not found.'];
                }
            }

            $vehicle->type_id = $data['type'];
            $vehicle->user_id = Auth::id();
            $vehicle->name = $data['name'];
            $vehicle->chassis_number = $data['chassis_number'];
            $vehicle->plate_number = $data['plate_number'];
            $vehicle->description = $data['description'];
            $vehicle->per_hour = $data['per_hour'];
            $vehicle->per_day = $data['per_day'];
            $vehicle->per_week = $data['per_week'];
            $vehicle->image = '';
            $vehicle->save();

            //Feature
            $featureIDs = array();
            $featureData = $data['feature'];
            if(is_array($featureData)) {
                foreach ($data['feature'] as $name) {
                    $feature = $type == 'create' ? Feature::firstOrCreate(['name' => trim($name)]) : Feature::firstOrNew(['name' => trim($name)]);
                    $featureIDs[] = $feature->id;
                }
            } else {
                $feature = $type == 'create' ? Feature::firstOrCreate(['name' => trim($featureData)]) : Feature::firstOrNew(['name' => trim($featureData)]);
                $featureIDs[] = $feature->id;
            }
            $vehicle->feature()->sync($featureIDs);

            //Slug
            $vehicle->slug = Str::slug($vehicle->id.' '.$vehicle->name, '-');
            $vehicle->save();

            $return = ['success' => true, 'data' => $vehicle, 'errors' => null, 'message' => 'Vehicle information successfully saved.'];
        }

        return $return;
    }

} 