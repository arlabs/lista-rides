<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model{

    protected $fillable = ['name','image','slug'];

    public function vehicle(){
        return $this->hasMany('App\Vehicle','type_id','id');
    }

} 