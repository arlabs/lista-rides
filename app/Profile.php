<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model{

    protected $fillable = ['user_id','first_name','last_name','gender','mobile','address','image'];

    public function user(){
        return $this->belongsTo('App\User');
    }

} 