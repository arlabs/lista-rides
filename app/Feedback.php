<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model{

    protected $fillable = ['user_id','account_type','rating','message'];

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }

} 