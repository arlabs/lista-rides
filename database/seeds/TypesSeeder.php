<?php

use App\Type;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TypesSeeder extends Seeder {

    public function run(){
        DB::table('types')->truncate();

        $types = [
            [
                'name' => 'Bicycle',
                'slug' => Str::slug('1 bicycle','-'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'name' => 'Motorcycle',
                'slug' => Str::slug('2 motorcycle','-'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'name' => 'Tricycle',
                'slug' => Str::slug('3 tricycle','-'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'name' => 'Multicab',
                'slug' => Str::slug('4 multicab','-'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'name' => 'Car',
                'slug' => Str::slug('5 car','-'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'name' => 'Van',
                'slug' => Str::slug('6 van','-'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'name' => 'Truck',
                'slug' => Str::slug('7 truck','-'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'name' => 'Tartanilya',
                'slug' => Str::slug('8 tartanilya','-'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ]
        ];

        Type::insert($types);
    }

}