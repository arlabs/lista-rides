<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPricesToVehicles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vehicles', function(Blueprint $table){
            $table->integer('per_hour')->after('description')->nullable();
            $table->integer('per_day')->after('per_hour')->nullable();
            $table->integer('per_week')->after('per_day')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('vehicles',function(Blueprint $table){
            $table->dropColumn('per_hour');
            $table->dropColumn('per_day');
            $table->dropColumn('per_week');
        });
	}

}
