<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquiriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inquiries', function(Blueprint $table){
            $table->increments('id');
            $table->integer('vehicle_id');
            $table->integer('renter_id');
            $table->dateTime('start');
            $table->dateTime('end');
            $table->string('price_type');
            $table->enum('status',['pending','accepted','declined','partially_returned','returned']);
            $table->timestamps();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inquiries');
	}

}
