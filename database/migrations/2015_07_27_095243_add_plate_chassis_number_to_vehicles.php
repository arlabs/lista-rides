<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlateChassisNumberToVehicles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vehicles',function(Blueprint $table){
            $table->string('chassis_number')->after('per_week')->nullable();
            $table->string('plate_number')->after('chassis_number')->nullable();
            $table->tinyInteger('verified')->after('image');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('vehicles',function(Blueprint $table){
            $table->dropColumn('chassis_number');
            $table->dropColumn('plate_number');
            $table->dropColumn('verified');
        });
	}

}
