{!! Form::open(['url' => 'feedback/store', 'id' => 'form-send-feedback']) !!}
<div class="modal-body">
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Important!</strong> We require you to send your feedback so that we will know how your transaction has gone [might change].
    </div>
    <div class="form-group">
        <span class="label bg-danger error-handler">&nbsp;</span>
        <label>Rating:</label>
        <div id="stars" class="starrr lead"></div>
        {!! Form::hidden('rating',null,['id' => 'rate_count']) !!}
    </div>
    <div class="form-group">
        <span class="label bg-danger error-handler">&nbsp;</span>
        <label>Message:</label>
        {!! Form::textarea('message',null,['class' => 'form-control required input-sm', 'rows' => '3']) !!}
    </div>
    {!! Form::hidden('id',$id) !!}
    {!! Form::hidden('status',$status) !!}
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-success btn-sm" data-loading-text="<i class='fa fa-spin fa-spinner'></i> Sending"
        data-original-text="Send Feedback">Send Feedback</button>
</div>
{!! Form::close() !!}

<script src="/backend/js/custom.js"></script>
<script src="/backend/js/feedback.js"></script>