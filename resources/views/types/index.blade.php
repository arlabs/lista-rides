@extends('layouts.frontend')

@section('content')

    <header id="head" class="secondary"></header>

    <div class="container">

        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Types</li>
        </ol>

        <div class="row">

            <!-- Article main content -->
            <article class="col-sm-12 maincontent">
                <header class="page-header">
                    <h1 class="page-title">Types</h1>
                </header>

                <div class="x_panel">
                    <div class="x_content">
                        @if($Types->count())
                            @foreach(array_chunk($Types->all(), 4) as $types)
                                <div class="row">
                                    @foreach($types as $type)
                                        <div class="col-xs-6 col-md-3">
                                            <div class="vehicle">
                                            <a href="{{ url('types/'.$type->slug) }}" class="thumbnail">
                                                <img src="/frontend/images/mac.jpg" alt="{{ ucwords($type->name) }}">
                                                <div class="vehicle-name">
                                                    <h4><strong>{{ ucwords($type->name) }}</strong></h4>
                                                </div>
                                            </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        @else
                            <h4>No available vehicle types</h4>
                        @endif

                        {!! $Types->render() !!}
                    </div>
                </div>
            </article>
            <!-- /Article -->

        </div>
    </div>
@endsection
