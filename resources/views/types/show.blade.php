@extends('layouts.frontend')

@section('content')

    <header id="head" class="secondary"></header>

    <div class="container">

        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{ url('types') }}">Types</a></li>
            <li class="active">{{ ucwords($Type->name) }}</li>
        </ol>

        <div class="row">

            <!-- Article main content -->
            <article class="col-sm-12 maincontent">
                <header class="page-header">
                    <h1 class="page-title">{{ ucwords($Type->name) }}</h1>
                </header>

                <div class="x_panel">
                    <div class="x_content">
                        @if($Vehicles->count())
                            @foreach(array_chunk($Vehicles->all(), 4) as $vehicles)
                                <div class="row">
                                    @foreach($vehicles as $vehicle)
                                        <div class="col-xs-6 col-md-3">
                                            <div class="vehicle">
                                                <div class="vehicle-owner">
                                                    <div class="pop-content">
                                                        <strong>{{ ucwords($vehicle->user->profile->first_name.' '.$vehicle->user->profile->last_name) }}</strong><br/>
                                                        <small><i class="fa fa-map-marker"></i> {{ ucwords($vehicle->user->profile->address) }}</small><br/>
                                                        <hr class="hr-sm"/>
                                                        <small>Rating: {!! $vehicle->user->getFeedbackAverageWithStar() !!}</small>
                                                    </div>

                                                    <img src="/img/{{ $vehicle->user->profile->image?: 'img.jpg' }}" class="img-responsive img-circle"/>
                                                </div>
                                                <a href="{{ url('vehicles/'.$vehicle->slug) }}" class="thumbnail">
                                                    <img src="/img/{{ $vehicle->image }}" alt="{{ ucwords($vehicle->name) }}">
                                                    <div class="vehicle-name">
                                                        <h4><strong>{{ ucwords($vehicle->name) }}</strong></h4>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        @else
                            <h4>No available vehicles</h4>
                        @endif

                        {!! $Vehicles->render() !!}
                    </div>
                </div>
            </article>
            <!-- /Article -->

        </div>
    </div>
@endsection
