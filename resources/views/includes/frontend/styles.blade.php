<link rel="shortcut icon" href="/frontend/images/gt_favicon.png">

<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
<link rel="stylesheet" href="/frontend/css/bootstrap.min.css">
<link rel="stylesheet" href="/frontend/css/font-awesome.min.css">

<!-- Custom styles for our template -->
<link rel="stylesheet" href="/frontend/css/bootstrap-theme.css" media="screen" >
<link rel="stylesheet" href="/frontend/css/main.css">

<!-- Date Time Picker -->
<link rel="stylesheet" href="/datepicker/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css">

<link rel="stylesheet" href="/css/global.css">
<link rel="stylesheet" href="/frontend/css/auth.css">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="/frontend/js/html5shiv.js"></script>
<script src="/frontend/js/respond.min.js"></script>
<![endif]-->

<script src="/frontend/js/jquery.min.js"></script>
<script src="/frontend/js/bootstrap.js"></script>