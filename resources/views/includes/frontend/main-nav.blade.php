<!-- Fixed navbar -->
<div class="navbar navbar-inverse navbar-fixed-top headroom" >
    <div class="container">
        <div class="navbar-header">
            <!-- Button for smallest screens -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="index.html"><img src="/frontend/images/logo.png" alt="Progressus HTML5 template"></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
                <li class="{{ Active::pattern('/') }}"><a href="{{ url('/') }}">Home</a></li>
                <li class="{{ Active::pattern(['types','types/*'],'active') }}"><a href="{{ url('types') }}">Browse & Inquire</a></li>
                @if(Auth::guest())
                    <li class="{{ Active::pattern(['auth/register'],'active') }}"><a href="{{ url('auth/register') }}">List My Ride</a></li>
                    <li><a class="btn" href="{{ url('auth/login') }}">SIGN IN</a></li>
                @else
                    @if(Auth::user()->account_type == 'admin')
                        @include('modules.frontend.admin')
                    @else
                        @include('modules.frontend.user')
                    @endif
                    <li><a class="btn" href="{{ url('auth/login') }}">LOG OUT</a></li>
                @endif
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>
<!-- /.navbar -->