<!-- Header -->
<header id="head">
    <div class="container">
        <div class="row">
            <h1 class="lead">LISTA RIDES</h1>
            <p class="tagline">Duis est sapien, congue in elementum egestas, egestas et lacus.</p>
            <p><a href="{{ url('types') }}" class="btn btn-default btn-lg" role="button">I WANT TO RENT</a> <a href="{{ url('auth/register') }}" class="btn btn-action btn-lg" role="button">LIST MY RIDE</a></p>
        </div>
    </div>
</header>
<!-- /Header -->