<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
<script src="/frontend/js/headroom.min.js"></script>
<script src="/frontend/js/jQuery.headroom.min.js"></script>
<script src="/frontend/js/template.js"></script>

<!-- Date Time Picker -->
<script src="/datepicker/moment.js"></script>
<script src="/datepicker/bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- PNotify -->
<script type="text/javascript" src="/backend/js/notify/pnotify.core.js"></script>
<script type="text/javascript" src="/backend/js/notify/pnotify.buttons.js"></script>
<script type="text/javascript" src="/backend/js/notify/pnotify.nonblock.js"></script>

<script src="/js/global.js"></script>
<script src="/backend/js/inquiry.js"></script>

<script>
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="_token"]').attr('content')
            }
        });
    });
</script>