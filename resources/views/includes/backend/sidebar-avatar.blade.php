<div class="navbar nav_title">
    <a href="{{ url('home') }}" class="site_title"><i class="fa fa-car"></i> <span>Project Name</span></a>
</div>
<div class="clearfix"></div>

<!-- menu prile quick info -->
<div class="profile">
    <div class="profile_pic">
        <img src="/img/{{ Auth::user()->profile->image?: 'img.jpg' }}" alt="..." class="img-circle profile_img">
    </div>
    <div class="profile_info">
        <span>Welcome,</span>
        <h2>{{ ucwords(Auth::user()->profile->first_name.' '.Auth::user()->profile->last_name) }}</h2>
    </div>
</div>
<!-- /menu prile quick info -->