<script src="/backend/js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="/backend/js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="/backend/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="/backend/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="/backend/js/icheck/icheck.min.js"></script>

<script src="/backend/js/custom.js"></script>

<!-- PNotify -->
<script type="text/javascript" src="/backend/js/notify/pnotify.core.js"></script>
<script type="text/javascript" src="/backend/js/notify/pnotify.buttons.js"></script>
<script type="text/javascript" src="/backend/js/notify/pnotify.nonblock.js"></script>

<!-- Date Time Picker -->
<script src="/datepicker/moment.js"></script>
<script src="/datepicker/bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" src="/js/global.js"></script>
<script type="text/javascript" src="/backend/js/profile.js"></script>
<script type="text/javascript" src="/backend/js/vehicle.js"></script>
<script type="text/javascript" src="/backend/js/inquiry.js"></script>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name="_token"]').attr('content')
                }
            });
        });
    </script>