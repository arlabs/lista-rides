<!-- Bootstrap core CSS -->
<link href="/backend/css/bootstrap.min.css" rel="stylesheet">
<link href="/backend/fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="/backend/css/animate.min.css" rel="stylesheet">

<!-- Custom styling plus plugins -->
<link href="/backend/css/custom.css" rel="stylesheet">
<link href="/backend/css/icheck/flat/green.css" rel="stylesheet">

<!-- Date Time Picker -->
<link rel="stylesheet" href="/datepicker/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css">

<link href="/css/global.css" rel="stylesheet">
<link href="/backend/css/profile.css" rel="stylesheet">

<!-- select2 -->
<link href="/backend/css/select/select2.min.css" rel="stylesheet">


<script src="/backend/js/jquery.min.js"></script>

<!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->