@extends('layouts.frontend')

@section('content')

    <header id="head" class="secondary"></header>

    <div class="container">

        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Registration</li>
        </ol>

        <div class="row">

            <!-- Article main content -->
            <article class="col-xs-12 maincontent">
                <header class="page-header-auth">
                    <h1 class="page-title">Registration</h1>
                </header>

                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p class="text-center text-muted">Already have an account? <a href="{{ url('auth/login') }}">Sign In</a></p>
                            <hr>

                            <form method="POST" action="/auth/register">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="top-margin form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                    <span class="label bg-danger error-handler">&nbsp;</span>
                                    {!! $errors->first('first_name', '<span class="label bg-danger error-handler with-error">:message</span>') !!}
                                    <label>First Name:</label>
                                    <input type="text" name="first_name" class="form-control required" value="{{ old('first_name') }}" title="first name">
                                </div>
                                <div class="top-margin form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                    <span class="label bg-danger error-handler">&nbsp;</span>
                                    {!! $errors->first('last_name', '<span class="label bg-danger error-handler with-error">:message</span>') !!}
                                    <label>Last Name:</label>
                                    <input type="text" name="last_name" class="form-control required" value="{{ old('last_name') }}" title="last name">
                                </div>
                                <div class="top-margin form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
                                    <span class="label bg-danger error-handler">&nbsp;</span>
                                    {!! $errors->first('gender', '<span class="label bg-danger error-handler with-error">:message</span>') !!}
                                    <label>Gender:</label>
                                    {!! Form::select('gender', ['male' => 'Male', 'female' => 'Female'], old('gender'), ['class' => 'form-control required', 'title' => 'gender']) !!}
                                </div>
                                <div class="top-margin form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
                                    <span class="label bg-danger error-handler">&nbsp;</span>
                                    {!! $errors->first('mobile', '<span class="label bg-danger error-handler with-error">:message</span>') !!}
                                    <label>Mobile:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">+63</div>
                                        <input type="text" name="mobile" class="form-control required number" value="{{ old('mobile') }}" title="mobile" maxlength="10">
                                    </div>
                                </div>
                                <div class="top-margin form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                                    <span class="label bg-danger error-handler">&nbsp;</span>
                                    {!! $errors->first('address', '<span class="label bg-danger error-handler with-error">:message</span>') !!}
                                    <div class="arrow-down"></div>
                                    <label>Address:</label>
                                    <input type="text" name="address" class="form-control required" value="{{ old('address') }}" title="address">
                                </div>

                                <div class="top-margin form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <span class="label bg-danger error-handler">&nbsp;</span>
                                    {!! $errors->first('email', '<span class="label bg-danger error-handler with-error">:message</span>') !!}
                                    <label>Email:</label>
                                    <input type="email" name="email" class="form-control email" value="{{ old('email') }}" title="email">
                                </div>
                                <div class="top-margin form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                    <span class="label bg-danger error-handler">&nbsp;</span>
                                    {!! $errors->first('password', '<span class="label bg-danger error-handler with-error">:message</span>') !!}
                                    <label>Password:</label>
                                    <input type="password" name="password" class="form-control required" title="password">
                                </div>
                                <div class="top-margin form-group {{ $errors->has('confirm_password') ? 'has-error' : '' }}">
                                    <span class="label bg-danger error-handler">&nbsp;</span>
                                    {!! $errors->first('confirm_password', '<span class="label bg-danger error-handler with-error">:message</span>') !!}
                                    <label>Confirm Password:</label>
                                    <input type="password" name="confirm_password" class="form-control required" title="confirm password">
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="checkbox">
                                            <input type="checkbox">
                                            I've read the <a href="page_terms.html">Terms and Conditions</a>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 text-right">
                                        <button class="btn btn-action" type="submit">Register</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </article>
            <!-- /Article -->

        </div>
    </div>
@endsection
