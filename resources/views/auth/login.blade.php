@extends('layouts.login')

@section('content')
    <img src="/frontend/images/bg_header.jpg" class="bg"/>

    <!-- container -->
    <div class="container">

        <div class="row">

            <!-- Article main content -->
            <article class="col-xs-12 maincontent">

                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
                    <div class="panel panel-auth">
                        <div class="panel-body">
                            <header class="page-header-auth">
                                <h1 class="page-title">Sign In</h1>
                            </header>
                            {{--<h3 class="thin text-center">Sign in to your account</h3>--}}
                            <p class="text-center text-muted">Don't have an account? <a href="{{ url('auth/register') }}">Register</a></p>

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> Problem logging in.
                                </div>
                            @endif

                            <div class="push-5"></div>

                            <form method="POST" action="/auth/login">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="top-margin form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <span class="label bg-danger error-handler">&nbsp;</span>
                                    {!! $errors->first('email', '<span class="label bg-danger error-handler with-error">:message</span>') !!}
                                    <input type="email" name="email" class="form-control email" value="{{ old('email') }}" title="email" placeholder="Email" autofocus>
                                    <i class="fa fa-envelope form-control-feedback"></i>
                                </div>
                                <div class="top-margin form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                                    <span class="label bg-danger error-handler">&nbsp;</span>
                                    {!! $errors->first('password', '<span class="label bg-danger error-handler with-error">:message</span>') !!}
                                    <input type="password" name="password" class="form-control required" title="password" placeholder="Password">
                                    <i class="fa fa-lock form-control-feedback"></i>
                                </div>

                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> Remember Me
                                        </label>
                                    </div>
                                </div>

                                <hr class="hr-auth">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <a href="/password/email">Forgot password?</a>
                                        <button class="btn btn-default pull-right" type="submit">Sign in</button>
                                    </div>
                                </div>
                            </form>
                            <a href="{{ url('loginWithFacebook') }}" class="hidden">Facebook</a>
                        </div>
                    </div>

                </div>

            </article>
            <!-- /Article -->

        </div>
    </div>	<!-- /container -->

@endsection
