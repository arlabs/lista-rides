@extends('layouts.backend')

@section('content')
    <div class="right_col right_col_height" role="main">
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Inquiries</li>
        </ol>

        <div class="page-title">
            <div class="title_left">
                <h3>Inquiries</h3>
            </div>

            <div class="title_right">
                <div class="form-group pull-right"></div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">
                    <div class="x_content">

                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Renter</th>
                                    <th>Vehicle</th>
                                    <th>Start Date (<em>borrow</em>)</th>
                                    <th>End Date (<em>return</em>)</th>
                                    <th>Price Type</th>
                                    <th>Price</th>
                                    <th>Status</th>
                                    <th>Date Inquired</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($inquiries->count())
                                    @foreach($inquiries as $inquiry)
                                        <tr>
                                            <td>{{ $inquiry->id }}</td>
                                            <td>{{ ucwords($inquiry->renter->profile->first_name) }}</td>
                                            <td>{{ ucwords($inquiry->vehicle->name) }}</td>
                                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $inquiry->start)->format('d M, Y h:i A') }}</td>
                                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $inquiry->end)->format('d M, Y h:i A') }}</td>
                                            <td>{{ ucwords(str_replace('_',' ',$inquiry->price_type)) }}</td>
                                            <td>&#8369;{{ $inquiry->getPrice() }}</td>
                                            <td>{{ ucwords(str_replace('_',' ',$inquiry->status)) }}</td>
                                            <td>{{ $inquiry->created_at->format('d M, Y h:i A') }}</td>
                                            <td>
                                                @if($inquiry->renter_id == Auth::id()) {{--I am renter--}}
                                                    @if($inquiry->status == 'accepted')
                                                        <a href="{{ url('feedback/create') }}" data-id="{{ $inquiry->id }}"
                                                            data-status="partially_returned" class="btn btn-info btn-xs inquiry-action"
                                                            data-loading-text="<i class='fa fa-spin fa-spinner'></i> Loading"
                                                            data-original-text="<i class='fa fa-refresh'></i> Return"><i class="fa fa-refresh"></i> Return</a>
                                                    @endif
                                                @else {{--I am lister--}}
                                                    @if($inquiry->status == 'pending') {{--The first action of lister--}}
                                                        <a href="{{ url('inquiries/'.$inquiry->id.'/action?status=accepted') }}" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Accept</a>
                                                        <a href="{{ url('inquiries/'.$inquiry->id.'/action?status=declined') }}" class="btn btn-default btn-xs"><i class="fa fa-times"></i> Decline</a>
                                                    @endif

                                                    @if($inquiry->status == 'partially_returned') {{--Partially returned by renter--}}
                                                        <a href="{{ url('feedback/create') }}" data-id="{{ $inquiry->id }}"
                                                            data-status="returned" class="btn btn-info btn-xs inquiry-action"
                                                            data-loading-text="<i class='fa fa-spin fa-spinner'></i> Loading"
                                                            data-original-text="<i class='fa fa-thumbs-up'></i> Confirm Return"><i class="fa fa-thumbs-up"></i> Confirm Return</a>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr><td class="text-center" colspan="9">No inquiries available</td></tr>
                                @endif
                            </tbody>
                        </table>

                        {!! $inquiries->render() !!}

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection