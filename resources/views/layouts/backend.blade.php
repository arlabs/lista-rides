<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}" />

    <title>Gentallela Alela! | </title>

    @include('...includes.backend.styles')
    @yield('styles')

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col">
                    @include('...includes.backend.sidebar-avatar')

                    <br />

                    @include('...includes.backend.sidebar-menu')
                </div>
            </div>

            @include('...includes.backend.top-nav')

            <!-- pusher -->
            <div class="push-right-col"></div>

            <!-- Flash message -->
            <div class="right_col" role="main">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success alert-dismissible fade in hidden" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong>Holy guacamole!</strong> Best check yo self, you're not looking too good.
                        </div>

                        @include('flash::message')
                    </div>
                </div>
            </div>
            <!-- /Flash message -->

            <!-- page content -->
            @yield('content')
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    @include('includes.modals')

    @include('...includes.backend.scripts')
    @yield('scripts')

</body>

</html>