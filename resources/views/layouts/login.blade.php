<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="Sergey Pozhilov (GetTemplate.com)">

	<title>Progressus - Free business bootstrap template by GetTemplate</title>

	@include('includes.frontend.styles')
</head>

<body class="home">

	@yield('content')

    @include('includes.frontend.scripts')

</body>
</html>