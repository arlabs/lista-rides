<li class="{{ Active::pattern(['vehicles','vehicles/*'], 'current-page') }}"><a href="{{ url('vehicles') }}">My Vehicles</a></li>
<li class="{{ Active::pattern(['inquiries','inquiries/*'], 'current-page') }}"><a href="{{ url('inquiries ') }}">Inquiries</a></li>

{{--<li class="dropdown">--}}
    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">More Pages <b class="caret"></b></a>--}}
    {{--<ul class="dropdown-menu">--}}
        {{--<li><a href="sidebar-left.html">Left Sidebar</a></li>--}}
        {{--<li class="active"><a href="sidebar-right.html">Right Sidebar</a></li>--}}
    {{--</ul>--}}
{{--</li>--}}