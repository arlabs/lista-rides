<li class="{{ Active::pattern(['vehicles','vehicles/*'], 'current-page') }}"><a href="{{ url('vehicles') }}"><i class="fa fa-car"></i> My Vehicles</a>
</li>
<li class="{{ Active::pattern(['types','types/*'], 'current-page') }}"><a href="{{ url('types') }}"><i class="fa fa-globe"></i> Browse & Inquire</a>
</li>
<li class="{{ Active::pattern(['inquiries','inquiries/*'], 'current-page') }}"><a href="{{ url('inquiries') }}"><i class="fa fa-question-circle"></i> Inquiries</a>
</li>

{{--<li><a><i class="fa fa-car"></i> Vehicles <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu" style="display: none;">
        <li class="{{ Active::pattern(['vehicles','vehicles/*'], 'current-page') }}"><a href="{{ url('vehicles') }}">My Vehicles</a>
        </li>
        <li class="{{ Active::pattern(['types','types/*'], 'current-page') }}"><a href="{{ url('types') }}">Browse & Rent</a>
        </li>
    </ul>
</li>--}}