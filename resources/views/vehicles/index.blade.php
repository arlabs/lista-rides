@extends('layouts.backend')

@section('content')
    <div class="right_col right_col_height" role="main">
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Vehicles</li>
        </ol>

        <div class="page-title">
            <div class="title_left">
                <h3>Vehicles</h3>
            </div>

            <div class="title_right">
                <div class="form-group pull-right">
                    @if(Auth::user()->account_type == 'user')
                    <a href="{{ url('vehicles/create') }}" class="btn btn-success btn-sm btn-create-edit-vehicle"
                    title="Create" data-loading-text="<i class='fa fa-spin fa-spinner'></i> Loading"
                    data-original-text="List a Vehicle">List a Vehicle</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">
                    <div class="x_content">
                        @if(Auth::user()->account_type == 'user')
                        <div class="alert alert-info">
                            <a href="#read-more-vehicle-info" class="pull-right" data-toggle="collapse" data-target="#read-more-vehicle-info">Read more...</a>
                            <strong>Important!</strong>
                            Vehicle with <i class="fa fa-exclamation-circle"></i> sign indicates that the vehicle is unverified and will not be seen publicly.
                            We need to verify your vehicle first to ensure that it is secure to use.
                            <div id="read-more-vehicle-info" class="collapse">
                                In order for your vehicle to be verified and will be seen by the renters, all you have to do is simply complete the vehicle details when you list your vehicle.
                                That way, our Review & Evaluations Team could conveniently review/evaluate your vehicle details and will verify it if it passes the evaluation process.
                            </div>
                        </div>
                        @endif

                        @if($Vehicles->count())
                            @foreach(array_chunk($Vehicles->all(), 4) as $vehicles)
                                <div class="row">
                                    @foreach($vehicles as $vehicle)
                                        <div class="col-md-3">
                                            <div class="thumbnail">
                                                <div class="image view view-first">
                                                    <img style="width: 100%; display: block;" src="/img/{{ $vehicle->image }}" alt="image" />
                                                    <div class="mask">
                                                        <p>{{ $vehicle->name }}</p>
                                                        <div class="tools tools-bottom">
                                                            @if(Auth::user()->account_type == 'user')
                                                            <a href="{{ url('vehicles/'.$vehicle->slug.'/edit') }}" class="btn-create-edit-vehicle"
                                                                title="Edit" data-loading-text="<i class='fa fa-spin fa-spinner'></i>"
                                                                data-original-text="<i class='fa fa-pencil'></i>"><i class="fa fa-pencil"></i></a>
                                                            <a href="{{ url('uploads/loader?slug='.$vehicle->slug.'&purpose=vehicle') }}"
                                                                class="btn-upload" data-loading-text="<i class='fa fa-spin fa-spinner'></i>"
                                                                data-original-text="<i class='fa fa-camera'></i>"><i class="fa fa-camera"></i></a>
                                                            @endif
                                                            <a href="#"><i class="fa fa-times"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="caption text-center">
                                                    {!! $vehicle->verification_status() !!}
                                                    <a href="{{ url('vehicles/'.$vehicle->slug) }}"><strong>{{ ucwords($vehicle->name) }}</strong></a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        @else
                            <h5>No vehicles available</h5>
                        @endif

                        {!! $Vehicles->render() !!}

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection