{!! Form::open(['url' => 'vehicles/'.$slug.'/verify', 'method' => 'post', 'id' => 'verification-form']) !!}
<div class="modal-body">
    Are you sure you want to {{ $request->get('action') }} this vehicle?
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-success btn-sm" data-action="{{ $request->get('action') }}" data-loading-text="<i class='fa fa-spin fa-spinner'></i> {{ str_replace('y','ying',ucwords($request->get('action'))) }}" data-original-text="{{ ucwords($request->get('action')) }}">{{ ucwords($request->get('action')) }}</button>
</div>
{!! Form::close() !!}

<script src="/backend/js/vehicle.js"></script>