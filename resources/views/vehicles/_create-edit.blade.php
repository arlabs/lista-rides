<style>
.select2-selection__choice {
    background-color: #18bc9c !important;
    color: #ffffff;
}
.select2-selection__choice__remove {
    color: #ffffff !important;
}
</style>

{!! Form::open(['url' => $url, 'method' => 'post', 'id' => 'form-create-edit-vehicle']) !!}
    <div class="modal-body">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div class="form-group">
                    <span class="label bg-danger error-handler">&nbsp;</span>
                    <label>Vehicle Type:</label>
                    {!! Form::select('type', $types, $vehicle ? $vehicle->type_id : null, ['class' => 'form-control input-sm required', 'title' => 'type']) !!}
                </div>
                <div class="form-group">
                    <span class="label bg-danger error-handler">&nbsp;</span>
                    <label>Vehicle Name:</label>
                    <input type="text" name="name" value="{{ $vehicle ? $vehicle->name : '' }}" class="form-control input-sm required" title="name">
                </div>
                <div class="form-group">
                    <span class="label bg-danger error-handler">&nbsp;</span>
                    <label>Chassis Number/Vehicle Identification Number (VIN):</label>
                    <input type="text" name="chassis_number" value="{{ $vehicle ? $vehicle->chassis_number : '' }}" class="form-control input-sm required" title="chassis number">
                </div>
                <div class="form-group">
                    <span class="label bg-danger error-handler">&nbsp;</span>
                    <label>Plate Number:</label>
                    <input type="text" name="plate_number" value="{{ $vehicle ? $vehicle->plate_number : '' }}" class="form-control input-sm required" title="plate number">
                </div>
                <div class="form-group">
                    <span class="label bg-danger error-handler">&nbsp;</span>
                    <label>Description:</label>
                    {!! Form::textarea('description', $vehicle ? $vehicle->description : null, ['class' => 'form-control input-sm required', 'rows' => 3, 'title' => 'description']) !!}
                </div>
                <div class="form-group">
                    <span class="label bg-danger error-handler">&nbsp;</span>
                    <label>Features:</label>
                    {{--{!! Form::select('', $features, null, ['class' => 'featureSet form-control input-sm required', 'style' => 'width: 100%;', 'title' => 'feature']) !!}--}}
                    <select name="feature" class="featureSet tags form-control col-md-12" style="width: 100%;" multiple="multiple">
                        @foreach($features as $feature)
                            <option value="{{ $feature->name }}">{{ ucwords($feature->name) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <section class="panel panel-default">
                    <div class="panel-heading"><h4 class="panel-title">Price</h4></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <span class="label bg-danger error-handler">&nbsp;</span>
                            <label>Per Hour:</label>
                            <div class="input-group">
                                <div class="input-group-addon">&#8369;</div>
                                {!! Form::text('per_hour', $vehicle ? $vehicle->per_hour : null, ['class' => 'form-control input-sm required', 'title' => 'per hour']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="label bg-danger error-handler">&nbsp;</span>
                            <label>Per Day:</label>
                            <div class="input-group">
                                <div class="input-group-addon">&#8369;</div>
                                {!! Form::text('per_day', $vehicle ? $vehicle->per_day : null, ['class' => 'form-control input-sm required', 'title' => 'per day']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="label bg-danger error-handler">&nbsp;</span>
                            <label>Per Week:</label>
                            <div class="input-group">
                                <div class="input-group-addon">&#8369;</div>
                                {!! Form::text('per_week', $vehicle ? $vehicle->per_week : null, ['class' => 'form-control input-sm required', 'title' => 'per week']) !!}
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-success btn-sm" data-loading-text="<i class='fa fa-spin fa-spinner'></i> Saving" data-original-text="Submit">Submit</button>
    </div>
{!! Form::close() !!}

<script src="/backend/js/vehicle.js"></script>

<!-- Select2 -->
<script src="/backend/js/tags/jquery.tagsinput.min.js"></script>
<script src="/backend/js/select/select2.full.js"></script>
<script>
    $(function () {
        var featureSet = $(".featureSet");
        featureSet.select2({
            tags: true,
            tokenSeparators: [',']
        });
        featureSet.select2("val", <?php echo json_encode($vehicleFeature) ?>);
    });
</script>