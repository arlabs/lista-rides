@extends($isOwnerOrAdmin ? 'layouts.backend' : 'layouts.frontend')

@section('content')

    <header id="head" class="secondary"></header>

    @if($isOwnerOrAdmin)
    <div class="right_col right_col_height">
    @else
    <div class="container">
    @endif

        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
            @if($isOwnerOrAdmin)
            <li><a href="{{ url('vehicles') }}">Vehicles</a></li>
            <li class="active">{{ ucwords($vehicle->name) }}</li>
            @else
            <li><a href="{{ url('types') }}">Types</a></li>
            <li><a href="{{ url('types/'.$vehicle->type->slug) }}">{{ ucwords($vehicle->type->name) }}</a></li>
            <li class="active">{{ ucwords($vehicle->name) }}</li>
            @endif
        </ol>

        <div class="row" role="main">

            <article class="col-sm-12 maincontent">
                @if($isOwnerOrAdmin)
                <div class="page-title">
                    <div class="title_left">
                        <h3>{!! $vehicle->verification_status() !!} {!! ucwords($vehicle->name . ' <small> - ' . $vehicle->type->name . '</small>') !!}</h3>
                    </div>

                    <div class="title_right">
                        <div class="form-group pull-right">
                            @if($vehicle->verified)
                                <a href="{{ url('vehicles/'.$vehicle->slug.'/verify') }}" data-action="unverify" class="btn btn-success btn-sm verify-vehicle" data-loading-text="<i class='fa fa-spin fa-spinner'></i> Loading" data-original-text="Unverify">Unverify</a>
                            @else
                                <a href="{{ url('vehicles/'.$vehicle->slug.'/verify') }}" data-action="verify" class="btn btn-success btn-sm verify-vehicle" data-loading-text="<i class='fa fa-spin fa-spinner'></i> Loading" data-original-text="Verify">Verify</a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                @else
                <header class="page-header">
                    <h1 class="page-title">{{ ucwords($vehicle->name) }}</h1>
                </header>
                @endif

                <div class="x_panel">
                    <div class="x_content">
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <ul class="nav nav-tabs bar_tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#photo" role="tab" data-toggle="tab" aria-controls="photo" aria-expanded="true">Photo</a>
                                    </li>
                                    <li role="presentation" class=""><a href="#description_feature" role="tab" data-toggle="tab" aria-controls="description_feature" aria-expanded="false">Description & Features</a>
                                    </li>
                                    <li role="presentation" class=""><a href="#calendar" role="tab" data-toggle="tab" aria-controls="calendar" aria-expanded="false">Calendar</a>
                                    </li>
                                    <li role="presentation" class=""><a href="#map" role="tab" data-toggle="tab" aria-controls="map" aria-expanded="false">Map</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="photo" aria-labelledby="photo-tab">
                                        <div class="product-image">
                                            <img src="/img/{{ $vehicle->image }}" alt="Vehicle" />
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="description_feature" aria-labelledby="description_feature-tab">
                                        <h4>Description:</h4>
                                        <p>{{ $vehicle->description }}</p>
                                        <hr class="hr-sm"/>
                                        <h4>Features:</h4>
                                        @if($vehicle->feature->count())
                                            @foreach($vehicle->feature as $feature)
                                                <span class="label bg-success" style="font-size: 100%;">{{ ucwords($feature->name) }}</span>
                                            @endforeach
                                        @else
                                            <p>No features listed</p>
                                        @endif
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="calendar" aria-labelledby="calendar-tab">
                                        Calendar
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="map" aria-labelledby="map-tab">
                                        Map
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5 col-sm-5 col-xs-12" style="border:0 solid #e5e5e5;">
                            {!! Form::open(['url' => 'inquiries/store', 'method' => 'post', 'id' => 'form-inquiry']) !!}
                            {!! Form::hidden('vehicle_id',$vehicle->id) !!}
                            <section class="panel panel-default">
                                <div class="panel-heading"><h4 class="panel-title">Price</h4></div>
                                <div class="push-xs"></div>
                                <div class="panel-body hidden"><div class="alert alert-danger"></div></div>
                                <div class="row text-center">
                                    <div class="col-xs-4 form-group">
                                        <p><strong>Per Hour</strong></p>
                                        <div class="radio">
                                            <label>
                                                @if(!$isOwnerOrAdmin)<input type="radio" class="flat" name="price_type" value="per_hour" data-amount="{{ number_format($vehicle->per_hour,2) }}" >@endif
                                                &#8369;{{ number_format($vehicle->per_hour,2) }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 form-group">
                                        <p><strong>Per Day</strong></p>
                                        <div class="radio">
                                            <label>
                                                @if(!$isOwnerOrAdmin)<input type="radio" class="flat" name="price_type" value="per_day" data-amount="{{ number_format($vehicle->per_day,2) }}">@endif
                                                &#8369;{{ number_format($vehicle->per_day,2) }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <p><strong>Per Week</strong></p>
                                        <div class="radio">
                                            <label>
                                                @if(!$isOwnerOrAdmin)<input type="radio" class="flat" name="price_type" value="per_week" data-amount="{{ number_format($vehicle->per_week,2) }}">@endif
                                                &#8369;{{ number_format($vehicle->per_week,2) }}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            @if(!$isOwnerOrAdmin)
                            <section class="panel panel-default">
                                <div class="panel-heading"><h4 class="panel-title">Schedule</h4></div>
                                <div class="panel-body row">
                                    <div class="col-xs-6 form-group">
                                        <span class="label bg-danger error-handler">&nbsp;</span>
                                        <label>Start</label>
                                        <div class="input-group date" id="start">
                                            <input type="text" name="start" class="form-control required input-sm" />
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 form-group">
                                        <span class="label bg-danger error-handler">&nbsp;</span>
                                        <label>End</label>
                                        <div class="input-group date" id="end">
                                            <input type="text" name="end" class="form-control required input-sm" />
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <div class="product_price">
                                <h1 class="price">&#8369;<span class="price-amount">{{ number_format($vehicle->per_hour,2) }}</span></h1>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-action btn-lg"
                                    data-loading-text="<i class='fa fa-spin fa-spinner'></i> Processing"
                                    data-original-text="Inquire this {{ ucwords($vehicle->type->name) }}">Inquire this {{ ucwords($vehicle->type->name) }}</button>
                            </div>
                            @endif
                            {!! Form::close() !!}

                            <hr/>
                            <div class="row">
                                <div class="col-sm-5">
                                    <img src="/img/{{ $vehicle->user->profile->image?: 'img.jpg' }}" class="img-responsive img-rounded" />
                                </div>
                                <div class="col-sm-7">
                                    <h2>{{ ucwords($vehicle->user->profile->first_name.' '.$vehicle->user->profile->last_name) }}</h2>
                                    <p><i class="fa fa-envelope"></i> {{ $vehicle->user->email }}</p>
                                    <p><i class="fa fa-phone"></i> +63{{ $vehicle->user->profile->mobile }}</p>
                                    <p><i class="fa fa-map-marker"></i> {{ ucwords($vehicle->user->profile->address) }}</p>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </article>

        </div>
    </div>
@endsection