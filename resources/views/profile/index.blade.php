@extends('layouts.backend')

@section('content')
    <div class="right_col" role="main">
        <div class="page-title">
            <div class="title_left">
                <h3>Profile Information</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    {{-- Action buttons, etc --}}
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="timeline-background">
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <div class="avatar-view" title="Change the avatar">
                                <img src="/img/{{ Auth::user()->profile->image?: 'img.jpg' }}" class="img-responsive" alt="Avatar">
                                <a href="{{ url('uploads/loader?slug='.Auth::user()->slug.'&purpose=profile') }}"
                                    class="btn btn-danger btn-xs btn-upload" data-loading-text="<i class='fa fa-spin fa-spinner'></i>"
                                    data-original-text="<i class='fa fa-camera'></i>"><i class="fa fa-camera"></i></a>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <div class="timeline-details">
                                <h3>{{ ucwords($user->profile->first_name.' '.Auth::user()->profile->last_name) }}</h3>

                                <ul class="list-unstyled user_data">
                                    <li><i class="fa fa-map-marker user-profile-icon"></i> {{ ucwords($user->profile->address) }}
                                    </li>
                                    <li><i class="fa fa-envelope user-profile-icon"></i> {{ $user->email }}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="x_panel">
                    <div class="x_content profile-below-content">
                        <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                            <div class="push-md"></div>
                            <div class="push-lg"></div>

                            <!--<a class="btn btn-success btn-block"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a>-->

                            <!-- Start profile completeness -->
                            <ul class="list-unstyled user_data">
                                <li>
                                    <p class="text-center pull-right">80%</p>
                                    <p>Profile Completeness</p>
                                    <div class="progress progress_sm">
                                        <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="80"></div>
                                    </div>
                                </li>
                                <li>
                                    <p class="pull-right"><a href="#" class="btn-change-password" title="Change password">change</a></p>
                                    <p>Password Strength</p>
                                    <div class="progress progress_sm">
                                        <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="97"></div>
                                    </div>
                                    <div class="panel panel-default hidden">
                                        <div class="panel-body">
                                            <h5>Change Password:</h5>
                                            {!! Form::open(['url' => 'profile/'.$user->slug.'/change_password', 'method' => 'post', 'id' => 'form-change-password']) !!}
                                                <div class="form-group">
                                                    <input type="password" name="current_password" class="form-control input-sm" placeholder="Current"/>
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" name="new_password" class="form-control input-sm" placeholder="New"/>
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" name="new_password" class="form-control input-sm" placeholder="Confirm"/>
                                                </div>
                                                <button type="submit" class="btn btn-success btn-xs pull-right">Change</button>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <!-- End profile completeness -->

                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <ul class="nav nav-tabs bar_tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#about" role="tab" data-toggle="tab" aria-controls="about" aria-expanded="true">About</a>
                                    </li>
                                    <li role="presentation" class=""><a href="#invites" role="tab" data-toggle="tab" aria-controls="invites" aria-expanded="false">Invites</a>
                                    </li>
                                    <li role="presentation" class=""><a href="#vehicles" role="tab" data-toggle="tab" aria-controls="vehicles" aria-expanded="false">Vehicles</a>
                                    </li>
                                </ul>
                                <div id="myTabContent2" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="about" aria-labelledby="about-tab">
                                        <a href="#" class="btn btn-success btn-xs form-edit-btn pull-right"><i class="fa fa-pencil"></i></a>
                                        <h4>Basic Information</h4><hr class="hr-sm"/>
                                        {!! Form::open(['url' => 'profile/'.$user->slug, 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'form-edit-profile']) !!}
                                            <div class="form-group">
                                                <label class="control-label col-md-2 col-sm-3 col-xs-12">First Name:</label>
                                                <div class="col-md-6 col-sm-7 col-xs-12">
                                                    <span class="label bg-danger error-handler">&nbsp;</span>
                                                    <input type="text" name="first_name" class="form-control required input-sm form-input hidden" value="{{ ucwords($user->profile->first_name) }}" title="first name">

                                                    <span class="form-text-static">{{ ucwords($user->profile->first_name) }}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Last Name:</label>
                                                <div class="col-md-6 col-sm-7 col-xs-12">
                                                    <span class="label bg-danger error-handler">&nbsp;</span>
                                                    <input type="text" name="last_name" class="form-control required input-sm form-input hidden" value="{{ ucwords($user->profile->last_name) }}" title="last name">

                                                    <span class="form-text-static">{{ ucwords($user->profile->last_name) }}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Gender:</label>
                                                <div class="col-md-6 col-sm-7 col-xs-12">
                                                    <div class="radio form-input hidden">
                                                        <label>
                                                            <input type="radio" class="flat" name="gender" value="male" {{ $user->profile->gender=='male'?'checked':'' }}> Male
                                                        </label>
                                                        <label>
                                                            <input type="radio" class="flat" name="gender" value="female" {{ $user->profile->gender=='female'?'checked':'' }}> Female
                                                        </label>
                                                    </div>

                                                    <span class="form-text-static">{{ ucwords($user->profile->gender) }}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Mobile:</label>
                                                <div class="col-md-6 col-sm-7 col-xs-12">
                                                    <span class="label bg-danger error-handler">&nbsp;</span>
                                                    <div class="input-group form-input hidden">
                                                        <div class="input-group-addon">+63</div>
                                                        <input type="text" name="mobile" class="form-control required number input-sm" maxlength="10" value="{{ ucwords($user->profile->mobile) }}" title="mobile">
                                                    </div>

                                                    <span class="form-text-static">+63{{ $user->profile->mobile }}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Address:</label>
                                                <div class="col-md-6 col-sm-7 col-xs-12">
                                                    <span class="label bg-danger error-handler">&nbsp;</span>
                                                    <input type="text" name="address" class="form-control required input-sm form-input hidden" value="{{ ucwords($user->profile->address) }}" title="address">

                                                    <span class="form-text-static">{{ ucwords($user->profile->address) }}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2 col-sm-3 col-xs-12"></label>
                                                <div class="col-md-6 col-sm-7 col-xs-12">
                                                    <div class="pull-right">
                                                        <button class="btn btn-success btn-xs form-done-btn hidden"
                                                            data-loading-text="<i class='fa fa-spin fa-spinner'></i> Saving"
                                                            data-original-text="<i class='fa fa-check'></i> Done"><i class="fa fa-check"></i> Done</button>
                                                        <button class="btn btn-default btn-xs form-cancel-btn hidden"><i class="fa fa-times"></i> Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="invites" aria-labelledby="invites-tab">
                                        <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip</p>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="vehicles" aria-labelledby="vehicles-tab">
                                        <p>xxFood truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('styles')
<style>
.form-group .error-handler.with-error {
    top: -28px;
    right: 10px;
}
</style>
@endsection