{!! Form::open(['url' => 'uploads/upload?slug='.$slug.'&purpose='.$purpose, 'files' => true, 'method' => 'post', 'id' => 'form-uploader']) !!}
<div class="modal-body">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {!! Form::file('image', array('id' => 'imgInput')) !!}
            </div>

            <div class="hidden" style="height: 100%;">
                <img id="origImg" src="#" alt="..." class="img-responsive" />
            </div>
        </div>
    </div>

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-success btn-sm" data-loading-text="<i class='fa fa-spin fa-spinner'></i> Uploading" data-original-text="Upload">Upload</button>
</div>
{!! Form::close() !!}

<script src="/js/global.js"></script>