$(function(){
    var input = $('.form-input');
    var doneBtn = $('.form-done-btn');
    var cancelBtn = $('.form-cancel-btn');
    var textStatic = $('.form-text-static');
    var editLink = $('.form-edit-btn');

    //region Edit profile manipulation (show/hide)
    editLink.on('click',function(e){
        e.preventDefault();

        $(this).addClass('hidden');
        input.removeClass('hidden');
        doneBtn.removeClass('hidden');
        cancelBtn.removeClass('hidden');
        textStatic.addClass('hidden');
    });
    //endregion

    //region Cancel edit
    cancelBtn.on('click',function(e){
        e.preventDefault();

        $(this).addClass('hidden');
        input.addClass('hidden');
        doneBtn.addClass('hidden');
        editLink.removeClass('hidden');
        textStatic.removeClass('hidden');
    });
    //endregion

    //region Insert profile manipulation
    var formEditProfile = $('#form-edit-profile');
    formEditProfile.submit(function(e){
        e.preventDefault();

        var btnDone = $('.form-done-btn');
        setBtnLoading(btnDone,true,true,false);

        $.ajax({
            url: formEditProfile.attr('action'),
            type: formEditProfile.attr('method'),
            data: {data: $(this).serializeObject()}
        })
            .always(function(result){
                if(result.success){
                    assignData(result.data,null);

                    editLink.removeClass('hidden');
                    input.addClass('hidden');
                    doneBtn.addClass('hidden');
                    cancelBtn.addClass('hidden');
                    textStatic.removeClass('hidden');

                    //PNotify
                    notify('Success!',result.message,result.type);
                } else {
                    if(result.errors != null) {
                        assignData(null,result.errors);
                    }
                    //PNotify
                    notify('Oops!',result.message,result.type);
                }

                setBtnLoading(btnDone,false,false,true);
            });
    });
    //endregion

    //region Change password
    var btnChangePassword = $('.btn-change-password');
    btnChangePassword.on('click',function(e){
        e.preventDefault();

        var passwordFormContent = $(this).closest('li').find('.panel');
        passwordFormContent.removeClass('hidden');
        passwordFormContent.addClass('animated flipInY');
    });

    var formChangePassword = $('#form-change-password');
    formChangePassword.submit(function(e){

    });
    //endregion
});


function assignData(data,errors)
{
    var first_name = $('input[name=first_name]');
    var last_name = $('input[name=last_name]');
    var gender = $('input[name=gender]');
    var mobile = $('input[name=mobile]');
    var address = $('input[name=address]');

    if(data != null) {
        first_name.val(properCase(data.profile.first_name)).closest('.form-group').find('.form-text-static').html(properCase(data.profile.first_name));
        first_name.closest('.form-group').removeClass('has-error').find('.error-handler').removeClass('with-error');

        last_name.val(properCase(data.profile.last_name)).closest('.form-group').find('.form-text-static').html(properCase(data.profile.last_name));
        last_name.closest('.form-group').removeClass('has-error').find('.error-handler').removeClass('with-error');

        gender.val(properCase(data.profile.gender)).closest('.form-group').find('.form-text-static').html(properCase(data.profile.gender));
        gender.closest('.form-group').removeClass('has-error').find('.error-handler').removeClass('with-error');

        mobile.val(data.profile.mobile).closest('.form-group').find('.form-text-static').html('+63'+data.profile.mobile);
        mobile.closest('.form-group').removeClass('has-error').find('.error-handler').removeClass('with-error');

        address.val(properCase(data.profile.address)).closest('.form-group').find('.form-text-static').html(properCase(data.profile.address));
        address.closest('.form-group').removeClass('has-error').find('.error-handler').removeClass('with-error');
    }
    if(errors != null){
        if(errors.first_name) { setErrors(errors.first_name,'first_name'); }
        if(errors.last_name) { setErrors(errors.last_name,'last_name'); }
        if(errors.gender) { setErrors(errors.gender,'gender'); }
        if(errors.mobile) { setErrors(errors.mobile,'mobile'); }
        if(errors.address) { setErrors(errors.address,'address'); }
    }
}