$(function (e) {
    //region Create/Edit Load Modal
    var btn = $('.btn-create-edit-vehicle');
    btn.on('click', function (e) {
        e.preventDefault();

        var $this = $(this);
        var href = $this.attr('href');

        setBtnLoading($this,true,true,false);

        $.ajax({
            url: href,
            type: "GET"
        })
            .always(function (result) {
                modalTitle.html($this.attr('title') + ' a Vehicle');
                modalDefaultBody.html(result);
                modalDefault.modal('show');

                setBtnLoading($this,false,false,true);
            });
    });

    var form = $('#form-create-edit-vehicle');
    form.submit(function (e) {
        e.preventDefault();

        var submit_btn = $(this).find('button[type=submit]');
        var cancel_btn = $(this).find('button[type=button]');
        setBtnLoading(submit_btn,true,true);
        setBtnLoading(cancel_btn,false,true);

        var action = $(this).attr('action');
        var method = $(this).attr('method');

        $.ajax({
            url: action,
            type: method,
            data: {data: $(this).serializeObject()}
        })
            .always(function (result) {
                setBtnLoading(submit_btn,false,false,true);
                setBtnLoading(cancel_btn,false,false,true);

                console.log(result);

                if(result.success) {
                    $('.modal-default').modal('hide');
                    notify('Success!',result.message,'success',3000);
                } else {
                    if(result.errors){
                        if(result.errors.name) { setErrors(result.errors.name,'name'); }
                        if(result.errors.type){ setErrors(result.errors.type,'type'); }
                        if(result.errors.chassis_number){ setErrors(result.errors.chassis_number,'chassis_number'); }
                        if(result.errors.plate_number){ setErrors(result.errors.plate_number,'plate_number'); }
                        if(result.errors.description){ setErrors(result.errors.description,'description'); }
                        if(result.errors.per_hour){ setErrors(result.errors.per_hour,'per_hour'); }
                        if(result.errors.per_day){ setErrors(result.errors.per_day,'per_day'); }
                        if(result.errors.per_week){ setErrors(result.errors.per_week,'per_week'); }
                    } else {
                        notify('Error!',result.message,'error');
                    }
                }
            });
    });
    //endregion
    
    //region Verify
    var verify = $('.verify-vehicle');
    verify.on('click', function (e) {
        e.preventDefault();

        var $this = $(this);
        var href = $this.attr('href');
        var action = $this.data('action');

        setBtnLoading($this,true,true,false);
        
        $.ajax({
            url: href,
            type: "GET",
            data: {action: action}
        })
            .always(function (result) {
                modalTitle.html('Verify a Vehicle');
                modalSmallBody.html(result);
                modalSmall.modal('show');

                setBtnLoading($this,false,false,true);
            });
    });

    $('#verification-form').submit(function (e) {
        e.preventDefault();

        var submit_btn = $(this).find('button[type=submit]');
        var cancel_btn = $(this).find('button[type=button]');
        setBtnLoading(submit_btn,true,true);
        setBtnLoading(cancel_btn,false,true);

        var url = $(this).attr('action');
        var method = $(this).attr('method');

        $.ajax({
            url: url,
            type: method,
            data: {action: submit_btn.data('action')}
        })
            .always(function (result) {
                setBtnLoading(submit_btn,false,false,true);
                setBtnLoading(cancel_btn,false,false,true);

                if(result.success){
                    $('.modal-small').modal('hide');
                    notify('Success!',result.message,'success',3000);
                } else {
                    notify('Error!',result.message,'error');
                }
            });
    });
    //region
});