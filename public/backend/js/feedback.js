$(function () {
    $('#form-send-feedback').submit(function (e) {
        e.preventDefault();

        var submit_btn = $(this).find('button[type=submit]');
        var cancel_btn = $(this).find('button[type=button]');
        setBtnLoading(submit_btn,true,true);
        setBtnLoading(cancel_btn,false,true);

        var action = $(this).attr('action');
        var method = $(this).attr('method');
        var data = $(this).serializeObject();

        $.ajax({
            url: action,
            type: method,
            data: data
        })
            .always(function (result) {
                setBtnLoading(submit_btn,false,false,true);
                setBtnLoading(cancel_btn,false,false,true);

                if(result.success) {
                    $('.modal-small').modal('hide');
                    notify('Success!',result.message,'success',3000);
                } else {
                    if(result.errors){
                        if(result.errors.rating) { setErrors(result.errors.rating,'rating'); }
                        if(result.errors.message){ setErrors(result.errors.message,'message'); }
                    } else {
                        notify('Error!',result.message,'error');
                    }
                }
            });
    });
});