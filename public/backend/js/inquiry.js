$(function () {
    var priceType = $('input[name=price_type]');
    priceType.on('click', function (e) {
        var amount = $(this).data('amount');

        $('.price-amount').html(amount);
    });

    //region Send inquiry
    var form_inquiry = $('#form-inquiry');
    form_inquiry.submit(function (e) {
        e.preventDefault();

        var action = $(this).attr('action');
        var method = $(this).attr('method');

        var btn = $(this).find('button[type=submit]');
        setBtnLoading(btn,true,true,false);

        $.ajax({
            url: action,
            type: method,
            data: $(this).serializeObject()
        })
            .always(function (result) {
                setBtnLoading(btn,false,false,true);

                console.log(result);

                if(result.success){
                    notify('Success!',result.message,'success',3000);
                } else {
                    if(result.errors){
                        if(result.errors.start) { setErrors(result.errors.start,'start'); }
                        if(result.errors.end) { setErrors(result.errors.end,'end'); }
                        if(result.errors.price_type) {
                            console.log(result.errors.price_type);
                            var $this = $('input[name=price_type]');
                            $this.closest('.panel').find('.panel-body').removeClass('hidden').find('.alert').html(result.errors.price_type);
                        }
                    } else {
                        notify('Error!',result.message,result.type);
                    }
                }
            });
    });
    //endregion

    //region Action
    var action = $('.inquiry-action');
    action.on('click', function (e) {
        e.preventDefault();

        var $this = $(this);
        var id = $this.data('id');
        var status = $this.data('status');
        var url = $this.attr('href');

        setBtnLoading($this,true,true,false);

        $.ajax({
            url: url,
            type: "GET",
            data: {id: id, status: status}
        })
            .always(function (result) {
                modalTitle.html('Send Feedback');
                modalSmallBody.html(result);
                modalSmall.modal('show');

                setBtnLoading($this,false,false,true);
            });
    });
    //endregion
});