var modalTitle = $('.modal-title');
var modalSmall = $('.modal-small');
var modalSmallBody = $('.modal-small-body');
var modalDefault = $('.modal-default');
var modalDefaultBody = $('.modal-default-body');
var modalLarge = $('.modal-large');
var modalLargeBody = $('.modal-large-body');

$(function(){
    $('[data-toggle="tooltip"]').tooltip();

    //region Number only
    $(".number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    //endregion

    //region Email validation
    function isValidEmail(email) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(email);
    }
    var email = $('.email');
    email.on('focusout',function(e){
        var $this = $(this);
        var title = $this.attr('title');
        var form_group = $this.parents('.form-group');
        var span = $this.closest('.form-group').find('.error-handler');

        if($.trim($this.val()) == '' || !isValidEmail($this.val())){
            span.html('Invalid email.');
            span.addClass('with-error');
            form_group.addClass('has-error');
        }else{
            span.removeClass('with-error');
            form_group.removeClass('has-error')
        }
    });
    //endregion

    //region Required field manipulation
    var required = $('.required');
    required.on('focusout',function(e){
        var $this = $(this);
        var title = $this.attr('title');
        var form_group = $this.parents('.form-group');
        var span = $this.closest('.form-group').find('.error-handler');

        if($.trim($this.val()) == ''){
            span.html((title ? 'The '+title : 'This') + ' field is required.');
            span.addClass('with-error');
            form_group.addClass('has-error');
        }else{
            span.removeClass('with-error');
            form_group.removeClass('has-error')
        }
    });
    //endregion

    //region Upload image
    var btnUpload = $('.btn-upload');
    btnUpload.on('click', function (e) {
        e.preventDefault();

        var $this = $(this);
        var href = $this.attr('href');

        setBtnLoading($this,true,true,false);

        $.ajax({
            url: href,
            type: "GET"
        })
            .always(function (result) {
                modalTitle.html('Upload Photo');
                modalSmallBody.html(result);
                modalSmall.modal('show');

                setBtnLoading($this,false,false,true);
            });
    });

    $("#imgInput").on('change', function(e){
        readUrlAndDisplay(this);
    });

    //region Process the upload
    var formUploader = $('#form-uploader');
    formUploader.submit(function(e) {
        e.stopPropagation();
        e.preventDefault();

        var submit_btn = $(this).find('button[type=submit]');
        var cancel_btn = $(this).find('button[type=button]');
        setBtnLoading(submit_btn,true,true);
        setBtnLoading(cancel_btn,false,true);

        var action = $(this).attr('action');
        var method = $(this).attr('method');

        // Create a formdata object and add the files
        var formData = new FormData($(this)[0]);

        $.ajax({
            url: action,
            type: method,
            data: formData,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false // Set content type to false as jQuery will tell the server its a query string request
        }).always(function(result) {
            setBtnLoading(submit_btn,false,false,true);
            setBtnLoading(cancel_btn,false,false,true);

            if(result.success){
                $('.modal-small').modal('hide');
                notify('Success!',result.message,'success',3000);
            }
            else {
                notify('Oops!',result.message,'error');
            }

        });
    });
    //endregion

    //region Date Time Picker
    $('#start, #end').datetimepicker({
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down",
            next: "fa fa-chevron-right",
            previous: "fa fa-chevron-left"
        }
    });
    //endregion

    //region Collapse
    $('a[data-toggle=collapse]').on('click', function (e) {
        e.preventDefault();
    });
    //endregion

    //region Popover - vehicle owner
    var popoverTemplate = '<div class="popover">' +
        '<div class="arrow"></div>' +
        '<div class="popover-content"></div>' +
        '</div>';

    $('.vehicle-owner img').popover({
        trigger: 'hover',
        placement: 'top',
        content: function () {
            var pop_content = $(this).closest('.vehicle-owner').find('.pop-content');
            return pop_content.html();
        },
        template: popoverTemplate,
        html: true
    });
    //endregion
});

function setErrors(ojb,name) {
    var $this = $('input[name='+name+'], select[name='+name+'], textarea[name='+name+']');
    $this.closest('.form-group').addClass('has-error').find('.error-handler').addClass('with-error').html(ojb);
}

function notify(title,text,type,delay){
    new PNotify({
        title: title,
        text: text,
        type: type
    });

    if(delay){
        $('.modal-small, .modal-default, .modal-large').on('hidden.bs.modal', function (e) {
            setTimeout(function() {
                location.reload();
            }, delay);
        });

        setTimeout(function() {
            location.reload();
        }, delay);
    }
}

function setBtnLoading(elem,load,disable,normalize){
    if(load){
        elem.html(elem.data('loading-text'));
    }
    if(disable){
        elem.addClass('disabled');
        elem.attr('disabled');
    }
    if(normalize){
        elem.html(elem.data('original-text'));
        elem.removeClass('disabled');
        elem.removeAttr('disabled');
    }
}

function properCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function readUrlAndDisplay(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var origImg = $('#origImg');
            origImg.parent().removeClass('hidden');
            origImg.attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};